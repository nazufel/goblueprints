package vault

import "context"

// Service provides pasword hasing capabilities
type Service interface {
	Hash(ctx context.Context, password string) (string, error)
	Validate(ctx context.Context, password, hash string) (bool, error)
}

type vaultService struct{}

// NewService makes a new Service
func NewService() Service {
	return vaultService{}
}
