package main

import (
	"encoding/json"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/garyburd/go-oauth/oauth"
	"github.com/joeshaw/envdecode"
)

var conn net.Conn

// dial the connection to Twitter
func dial(netw, addr string) (net.Conn, error) {
	// first check if the connection is closed. if not then close it.
	if conn != nil {
		conn.Close()
		conn = nil
	}

	// establish a new connection
	netc, err := net.DialTimeout(netw, addr, 5*time.Second)
	if err != nil {
		return nil, err
	}

	// assign the connection net.Conn type and return
	conn = netc
	return netc, nil
}

var reader io.ReadCloser

// close the connection to Twitter
func closeConn() {
	if conn != nil {
		conn.Close()
	}
	if reader != nil {
		reader.Close()
	}
}

var (
	authClient *oauth.Client
	creds      *oauth.Credentials
)

// setupTwitterAuth reads in environment variables and sets up oauth objects to be used
func setupTwitterAuth() {
	// init a struct of variables and look for the values in the environment
	// struct is initialized as a variable inside the function since these values won't be needed elsewhere.
	var ts struct {
		ConsumerKey    string `env:"SP_TWITTER_KEY,required"`
		ConsumerSecret string `env:"SP_TWITTER_SECRET,required"`
		AccessToken    string `env:"SP_TWITTER_ACCESSTOKEN,required"`
		AccessSecret   string `env:"SP_TWITTER_ACCESSSECRET,required"`
	}
	// fail if the environment variables aren't present
	if err := envdecode.Decode(&ts); err != nil {
		log.Fatalln(err)
	}

	creds = &oauth.Credentials{
		Token:  ts.AccessToken,
		Secret: ts.AccessSecret,
	}
	authClient = &oauth.Client{
		Credentials: oauth.Credentials{
			Token:  ts.ConsumerKey,
			Secret: ts.ConsumerSecret,
		},
	}
}

var (
	authSetupOnce sync.Once
	httpClient    *http.Client
)

func makeRequest(r *http.Request, p url.Values) (*http.Response, error) {
	// authSetuipOnce runs once from sync.Once so that it doesn't run everytime the makeRequest function is called
	authSetupOnce.Do(func() {
		setupTwitterAuth()
		httpClient = &http.Client{
			Transport: &http.Transport{
				Dial: dial,
			},
		}
	})

	formEnc := p.Encode()
	r.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Set("Content-Length", strconv.Itoa(len(formEnc)))
	r.Header.Set("Authorization", authClient.AuthorizationHeader(creds, "POST", r.URL, p))

	return httpClient.Do(r)
}

type tweet struct {
	Text string
}

func readFromTwitter(votes chan<- string) {
	log.Printf("made it to readfromtwitter")
	// load options from the polls data
	// options, err := loadOptions()
	// if err != nil {
	// 	log.Println("failed to load options:", err)
	// 	return
	// }
	options := make([]string, 4)
	options[0] = "happy"
	options[1] = "sad"
	options[2] = "fail"
	options[3] = "win"

	log.Printf("loaded options: %v", options)
	// create the url object parse
	u, err := url.Parse("https://stream.twitter.com/1.1/statuses/filter.json")
	if err != nil {
		log.Println("created filter request failed:", err)
		return
	}
	query := make(url.Values)
	query.Set("track", strings.Join(options, ", "))

	// build a new request
	r, err := http.NewRequest("POST", u.String(), strings.NewReader(query.Encode()))
	if err != nil {
		log.Println("creating filter request failed:", err)
		return
	}

	// use makeRequest to request data from Twitter
	resp, err := makeRequest(r, query)
	if err != nil {
		log.Println("making request failed:", err)
		return
	}

	// read the decode the reposne
	reader := resp.Body
	decoder := json.NewDecoder(reader)
	// loop over the decoded response
	for {
		var t tweet
		if err := decoder.Decode(&t); err != nil {
			break
		}
		for _, option := range options {
			if strings.Contains(
				strings.ToLower(t.Text),
				strings.ToLower(option),
			) {
				log.Println("vote:", option)
				// send data down the votes channel
				votes <- option
			}
		}
	}
}

// startTwitterSteam is function with a goroutine to call the readFromTwitter() function
func startTwitterStream(stopchan <-chan struct{}, votes chan<- string) <-chan struct{} {
	// make the payload for the stoppedchan for when the function exits
	stoppedchan := make(chan struct{}, 1)
	// start up a goroutine
	go func() {
		// defer sending the above payload on the stoppedchan for when the goroutine exits
		defer func() {
			stoppedchan <- struct{}{}
		}()
		// infinate loop of the goroutine to listen on two different channels
		for {
			select {
			// listen on channel "zero": is there a message to stop on the stopchan? yes, log and return out of the goroutine
			case <-stopchan:
				log.Println("stopping Twitter...")
				return
			// no message to stop, then log, call readFromTwitter() func, pass the votes channel.
			default:
				log.Println("Querying Twitter...")
				readFromTwitter(votes)
				// when the readFromTwitter function closes, i.e.: when Twitter closes the connection, wait ten seconds and start the loop over.
				log.Println(" (waiting)")
				time.Sleep(10 * time.Second) //wait before reconnecting
			}
		}
	}()
	// return the channel
	return stoppedchan
}
