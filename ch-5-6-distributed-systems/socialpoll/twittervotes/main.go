package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/bitly/go-nsq"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
)

func main() {
	var stoplock sync.Mutex // protects stop
	stop := false

	stopChan := make(chan struct{}, 1)
	signalChan := make(chan os.Signal, 1)
	go func() {
		<-signalChan
		stoplock.Lock()
		stop = true
		stoplock.Unlock()
		log.Println("Stopping...")
		stopChan <- struct{}{}
		closeConn()
	}()
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)
	if _, err := newStorage(); err != nil {
		log.Fatalf("failed to dail db: %v", err)
	}
	defer closedb()

	// start up

	// make the votes channel
	votes := make(chan string) //channel for votes
	publisherStoppedChan := publishVotes(votes)
	twitterStoppedChan := startTwitterStream(stopChan, votes)

	go func() {
		time.Sleep(1 * time.Minute)
		closeConn()
		stoplock.Lock()
		if stop {
			stoplock.Unlock()
			return
		}
		stoplock.Unlock()
	}()
	<-twitterStoppedChan
	close(votes)
	<-publisherStoppedChan
}

// Storage struct holds information about connecting to the DB
// type Storage struct {
// var db *mongo.Database
var client *mongo.Client = new(mongo.Client)

// }

// newStorage connects to the DB on start up
func newStorage() (*mongo.Client, error) {

	log.Println("pre client")

	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://root:example@localhost:27017")

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("post client: %v", client)

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Connected to MongoDB!")

	return client, nil
}

// // init the database session in a global
// var db *mgo.Session

// // get a database connection and assign to db variable
// func dialdb() error {
// 	var err error
// 	log.Println("dialing mongodb: localhost")

// 	if db, err := mgo.Dial("localhost"); err != nil {
// 		log.Fatalf("db dial error: %v", err)
// 	}

// 	log.Printf("db connection: %v", db)
// 	return err
// }

// close connetion to the database
func closedb() {
	client.Disconnect(context.Background())
	log.Println("closed database connection")
}

// data structure of what we will be retrieving from the db
type poll struct {
	Options []string
}

// query the database for options to scrape Twitter for

func loadOptions() ([]string, error) {
	log.Printf("loading options")

	var options []string
	type polls struct {
		title   string
		options []string
	}

	// poll := polls{title: "test poll"}

	filter := bson.D{{Name: "title", Value: "Test poll"}}

	log.Printf("before collection")
	collection := client.Database("ballots").Collection("polls")

	log.Printf("collection: %v", collection)

	cur, err := collection.Find(context.TODO(), filter)
	if err != nil {
		log.Printf("error retrieving options from database: %v", err)
	}
	defer cur.Close(context.Background())
	log.Printf("loaded options from the db.")

	for cur.Next(context.Background()) {
		var p polls

		err := cur.Decode(&p)
		if err != nil {
			log.Printf("unable to decode options from database: %v", err)
		}
		options = append(options, p.options...)
	}
	cur.Close(context.Background())
	log.Printf("loaded options: %v", options)
	return options, cur.Err()
}

//publishVotes func is a goroutine to that listens on the votes channel and publishes to an NSQ topic
func publishVotes(votes <-chan string) <-chan struct{} {
	// make the stopchan to return
	stopchan := make(chan struct{}, 1)
	// connection to NSQ
	pub, err := nsq.NewProducer("localhost:4150", nsq.NewConfig())
	if err != nil {
		log.Printf("error connecting to nsq: %v", err)
	}
	// start up a goroutine
	go func() {
		// loop over the votes channel and publish votes to NSQ
		for vote := range votes {
			pub.Publish("votes", []byte(vote)) //publish the vote
			log.Printf("published vote: %v", vote)
		}
		log.Println("Publisher: Stopping")
		// stop the nsq connection when the votes channel closes
		pub.Stop()
		log.Println("Publisher: Stopped")
		// send a message on the stopchan that this goroutine has stopped
		stopchan <- struct{}{}
	}()
	// return the channel
	return stopchan
}
