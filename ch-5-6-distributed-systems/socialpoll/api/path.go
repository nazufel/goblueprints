package main

import "strings"

// PathSeparator constant
const PathSeparator = "/"

// Path struct holds the path data
type Path struct {
	Path string
	ID   string
}

// NewPath parses the URL path
func NewPath(p string) *Path {
	var id string

	p = strings.Trim(p, PathSeparator)
	s := strings.Split(p, PathSeparator)
	// the last segment in the list is considered the ID of the resource we are trying to access
	if len(s) > 1 {
		id = s[len(s)-1]
		p = strings.Join(s[:len(s)-1], PathSeparator)
	}
	return &Path{Path: p, ID: id}
}

// HasID checks for the ID
func (p *Path) HasID() bool {
	return len(p.ID) > 0
}
