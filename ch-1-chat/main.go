package main

import (
	"flag"
	"html/template"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sync"

	"github.com/stretchr/gomniauth"
	"github.com/stretchr/gomniauth/providers/google"
	"github.com/stretchr/objx"
	"gitlab.com/ryanthebossross/goblueprints/chat/trace"
)

type templateHandler struct {
	once     sync.Once
	filename string
	templ    *template.Template
}

func (t *templateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	t.once.Do(func() {
		t.templ = template.Must(template.ParseFiles(filepath.Join("templates", t.filename)))
	})

	data := map[string]interface{}{
		"Host": r.Host,
	}

	if authCookie, err := r.Cookie("auth"); err == nil {
		log.Printf("authCooke: %v", authCookie)
		data["UserData"] = objx.MustFromBase64(authCookie.Value)
		log.Printf("UserData: %v", data["UserData"])
	}
	t.templ.Execute(w, data)
}

func main() {

	var addr = flag.String("addr", ":3000", "The addr of the application.")

	// parse the flags
	flag.Parse()
	gomniauth.SetSecurityKey("You Shall Pass")
	gomniauth.WithProviders(
		google.New("905979523719-v9v4b3e07rg1vou1hu0us53sfl033ge0.apps.googleusercontent.com", "S5ATFaS8UdbBp-SSsTuy0AjU", "http://localhost:3000/auth/callback/google"),
	)

	// init new room function
	r := newRoom()
	r.tracer = trace.New(os.Stdout)

	// MustAuth is helper function executed first, then if successful, runs the template handler
	http.HandleFunc("/", redirect)
	http.HandleFunc("/auth/", loginHandler)
	http.Handle("/chat", MustAuth(&templateHandler{filename: "chat.html"}))
	http.Handle("/login", &templateHandler{filename: "login.html"})
	http.HandleFunc("/logout", func(w http.ResponseWriter, r *http.Request) {
		http.SetCookie(w, &http.Cookie{
			Name: "auth",
			// setting the value of the cookie to empty since not all browsers actually remove the cookie data
			Value:  "",
			Path:   "/",
			MaxAge: -1,
		})
		w.Header().Set("Location", "/chat")
		w.WriteHeader(http.StatusTemporaryRedirect)
	})
	http.Handle("/room", r)
	// get the room going
	go r.run()

	log.Printf("server up and running at: %v", *addr)
	if err := http.ListenAndServe(*addr, nil); err != nil {
		log.Fatal("ListenAndService:", err)

	}
}

// redirect func redirects since there's nothing at the root route, for now
func redirect(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/chat", http.StatusTemporaryRedirect)
}
