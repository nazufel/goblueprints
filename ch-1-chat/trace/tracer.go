package trace

import (
	"fmt"
	"io"
)

// Tracer is an interface that describes and object capable of tracing events throughout the code
type Tracer interface {
	Trace(...interface{})
}

type tracer struct {
	out io.Writer
}

// New returns the interface when the struct was requested
func New(w io.Writer) Tracer {
	return &tracer{out: w}
}

func (t *tracer) Trace(a ...interface{}) {
	fmt.Fprint(t.out, a...)
	fmt.Fprintln(t.out)
}

type nilTracer struct{}

func (t *nilTracer) Trace(a ...interface{}) {}

func Off() Tracer {
	return &nilTracer{}
}
