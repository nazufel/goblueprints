package main

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
	"github.com/stretchr/objx"
	"gitlab.com/ryanthebossross/goblueprints/chat/trace"
)

type room struct {
	// forward is a channel that holds incoming messages
	// that should be forwarded to the other clients
	forward chan *message
	// join is a channel for clients wishing to join the room
	join chan *client
	// leave is a channel for clients wishing to leave the room
	leave chan *client
	// clients holds all currrent clients in this room
	clients map[*client]bool
	// tracer will recieve trance information of activity in the room
	tracer trace.Tracer
}

// newRoom makes a new room
func newRoom() *room {
	return &room{
		forward: make(chan *message),
		join:    make(chan *client),
		leave:   make(chan *client),
		clients: make(map[*client]bool),
		tracer:  trace.Off(),
	}
}

// infinite loop go routine that listens on all the channels
func (r *room) run() {
	for {
		select {
		// if a client sends a message on the join channel, then that client membership is marked as "true" and it will receive messages
		case client := <-r.join:
			// mark the client true
			r.clients[client] = true
			r.tracer.Trace("New client joined.")
		// if a client sends a leave message on the leave channel, it's removed from the map of clients
		case client := <-r.leave:
			// leaving
			delete(r.clients, client)
			r.tracer.Trace("Client left.")
		// if a message is written to the forward channel, then it's forwarded to all the subscribed clients
		case msg := <-r.forward:
			r.tracer.Trace("Message recieved: ", msg.Message)
			// forward message to all clients
			for client := range r.clients {
				client.send <- msg
				r.tracer.Trace(" --sent to client.")
			}
		}
	}
}

const (
	socketBufferSize    = 1024
	messssageBufferSize = 256
)

var upgrader = &websocket.Upgrader{ReadBufferSize: socketBufferSize, WriteBufferSize: socketBufferSize}

func (r *room) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	socket, err := upgrader.Upgrade(w, req, nil)
	if err != nil {
		log.Fatal("Failed to ServeHTTP:", err)
		return
	}
	authCookie, err := req.Cookie("auth")
	if err != nil {
		log.Fatal("Failed to find cookie:", err)
	}
	client := &client{
		socket:   socket,
		send:     make(chan *message, messssageBufferSize),
		room:     r,
		userData: objx.MustFromBase64(authCookie.Value),
	}

	r.join <- client
	defer func() { r.leave <- client }()
	go client.write()
	client.read()
}
